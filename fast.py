from fastapi import FastAPI, HTTPException
from pydantic import BaseModel, ValidationError
import mysql.connector

app = FastAPI()

class Product(BaseModel):
    id: int | None = None
    name: str
    description: str
    image: str
    cost: float

mydb_config = {
    "host": "localhost",
    "user": "benjamin",
    "password": "coucou",
    "database": "BDD_API"
}

mydb = mysql.connector.connect(**mydb_config)

@app.get("/product")
def get_product():
    with mydb.cursor(dictionary=True) as cursor:
        cursor.execute("SELECT * FROM Product")
        result = cursor.fetchall()
    return result


class Order(BaseModel):
    id: int | None = None
    product_id: int
    quantity: int
    total_cost: float

class Order_lines (BaseModel):
    order_id: int
    product_id:int
    qt: float

mydb = mysql.connector.connect(**mydb_config)

# Commande CLI pour récupérer les données de la table Order
@app.put("/order/lines")
def get_orders():
     with mydb.cursor(dictionary=True) as cursor:
        cursor.execute("SELECT * FROM Order_lines")
        result = cursor.fetchall()
        return result
    


# Ajout de lignes de commande
@app.post("/order/lines")
def add_order_lines(order_lines: list[Order_lines]):
    try:
        with mydb.cursor(buffered=True) as cursor:
            for line in order_lines:
                cursor.execute(
                    "INSERT INTO Order_lines (order_id, product_id, qt) VALUES (%s, %s, %s)",
                    )
            mydb.commit()
        return {"message": "Order lines added successfully"}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))

# Récupération des données de commande
@app.get("/order/{order_id}")
def get_order(order_id: int):
    cursor=mydb.cursor(dictionary=True)
    sql = "select * from `Order` where order_id=%s;"
    cursor.execute(sql, [order_id])
    data_order = cursor.fetchall()[0]
    return data_order

class Client(BaseModel):
    name: str
    phone: str
    email: str

@app.post("/client")
def get_client():
    with mydb.cursor(dictionary=True) as cursor:
        cursor.execute("SELECT * FROM Client")
        result = cursor.fetchall()
    return result

@app.post("/adress")
def get_adress():
    with mydb.cursor(dictionary=True) as cursor:
        cursor.execute("SELECT * FROM Address")
        result = cursor.fetchall()
    return result
    
@app.put("/order/{order_id}/{client_id}")
def get_order(order_id: int, client_id:int):
    cursor=mydb.cursor(dictionary=True)
    sql = "select * from `Order` where order_id=%s;"
    cursor.execute(sql, [order_id],[client_id])
    data_order = cursor.fetchall()[0]
    return data_order   






   

   




