SOURCE init.sql 

INSERT INTO Product(name, description, image, price, available)
values(
 'truc', 'Lorem Ipsum', 'https://picsum.photos/200', 200,1
);

INSERT INTO Product(name, description, image, price, available)
values(
    'bidule', 'Lorem merol', 'https://picsum.photos/100', 500,1
);


Select name, description, image from Product;

INSERT INTO `Order` VALUES();
SELECT * from `Order`;

INSERT INTO Order_lines(order_id,product_id,qt)
VALUES (1,1,3),
       (1,2,1),
       (2,1,1);
Select *from Order_lines;

INSERT INTO Client(name, phone, email)
values (
    'Bob', '0123456789', 'bob@gmail.com'
);
select * from Client;

INSERT INTO `Client` (name) 
VALUES(
    'Bob'
);
#SELECT * FROM Client;
#Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).
UPDATE `Order`
SET order_lines=1
WHERE order_lines=order_state;
#SELECT * from `Order`;

-- ### Associer Bob à la commande
UPDATE `Order`
SET client_id = 1
WHERE client_id = Order_id;
#SELECT * from `Order`;

INSERT INTO Address(street,city,zipcode,state,country)
values('12 street St','Schenectady','12345','New York','US'
);
select * from Address;

#Associer cette adresse comme facturation à la commande

UPDATE `Order`
SET client_id=1
WHERE client_id=order_state;
#SELECT * from `Order`;

#Créer l’adresse “12 boulevard de Strasbourg, 31000, Toulouse, OC, France”
INSERT INTO Address(street,city,zipcode,state,country)
values('12 boulevard de Strasbourg', 'Toulouse', '31000', 'Toulouse', 'France'
);
#select * from Address;

#Associer cette adresse à Bob.
UPDATE `Order`
SET client_id='Bob'
WHERE client_id=order_state;
#SELECT * from `Order`;

#Associer cette adresse comme adresse de livraison.
INSERT INTO Address(street,city,zipcode,state,country)
values('12 boulevard de Strasbourg', 'Toulouse', '31000', 'Toulouse', 'France'
);
#select * from Address;

#Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).

#SELECT * from `Order`;

#Associer le moyen de paiement “Card” à la commande.

UPDATE `Order`
SET payment_method = 'Credit Card'
WHERE order_id = 1;

#Select* From `Order`;

UPDATE `Order`
SET order_lines=1
WHERE order_lines=order_state;
SELECT * from `Order`;

-- Associer le moyen de paiement “Card” à la commande.
UPDATE `Order`
SET payment_method = 'Credit Card'               
WHERE order_id = 1;  

-- Passer la commande dans l’état suivant. 
UPDATE `Order` 
SET order_state = 'sent' 
WHERE order_id = 1;

-- -- Vérifier que la commande est dans l'état sent
SELECT * FROM `Order` WHERE order_id = 1 AND order_state = 'sent';