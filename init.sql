DROP DATABASE IF EXISTS BDD_API;
CREATE DATABASE BDD_API;
USE BDD_API;

CREATE TABLE Client(
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50),
    phone VARCHAR(50),
    email VARCHAR(255),
    default_address VARCHAR(255)
);

CREATE TABLE Address(
    address_id INT  AUTO_INCREMENT PRIMARY KEY,
    street VARCHAR(255),
    city VARCHAR(255),
    zipcode VARCHAR(10),
    state VARCHAR(255),
    country VARCHAR(255)
);
CREATE TABLE `Order`(
    order_id INT  AUTO_INCREMENT PRIMARY KEY,
    client_id INT,
    order_lines TEXT ,
    delivery_address INT ,  -- Changed to INT, as it refers to Address table
    billing_address INT ,   -- Changed to INT, as it refers to Address table
    payment_method ENUM('Credit Card','bank check on delivery','cash on delivery'),
    order_state ENUM('cart','validated','sent','delivered'),
    FOREIGN KEY (client_id) REFERENCES Client(id),
    FOREIGN KEY (delivery_address) REFERENCES Address(address_id),
    FOREIGN KEY (billing_address) REFERENCES Address(address_id)
);


CREATE TABLE Product(
    product_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description TEXT NOT NULL, 
    image VARCHAR(255) NOT NULL,
    price FLOAT NOT NULL,
    available BOOLEAN
);


CREATE TABLE Order_lines(
    order_id INT ,
    product_id INT ,
    qt FLOAT,
    PRIMARY KEY (order_id, product_id),  -- Added primary key
    FOREIGN KEY (order_id) REFERENCES `Order`(order_id),
    FOREIGN KEY (product_id) REFERENCES Product(product_id)
);

CREATE TABLE Client_address(
    client_id INT ,
    address_id INT,
    PRIMARY KEY (client_id, address_id),  -- Added primary key
    FOREIGN KEY (client_id) REFERENCES Client(id),
    FOREIGN KEY (address_id) REFERENCES Address(address_id)
);



INSERT INTO `Order` VALUES();
SELECT * from `Order`;

INSERT INTO Product(name, description, image, price, available)
values(
 'truc', 'Lorem Ipsum', 'https://picsum.photos/200', 200,1
);

INSERT INTO Product(name, description, image, price, available)
values(
    'bidule', 'Lorem merol', 'https://picsum.photos/100', 500,1
);


  
